<?php
/**
 * Created by PhpStorm.
 * User: markownik
 * Date: 08/04/17
 * Time: 13:09
 */

namespace lightupsolver;


abstract class FieldType
{
    const Normal = 0;
    const Black = 1;
}