<?php
/**
 * Created by PhpStorm.
 * User: markownik
 * Date: 08/04/17
 * Time: 16:57
 */

namespace lightupsolver;

include_once "GameState.class.php";


class AStarSolver
{
    private $search_space = array();
    private $visited = array();
    private $solution = array();

    public function __construct()
    {
    }

    public function addToSearch($state)
    {
        if(is_array($state))
            $this->search_space = array_merge($this->search_space, $state);
        else
            array_push($this->search_space, $state);
    }

    public function solution($state)
    {
        //solve the obvious fields
        $oldhelper = new GameState();
        $oldhelper->fromState($state);
        array_push($this->solution, $oldhelper);
//        $newhelper = new GameState();
//        $newhelper->fromState($oldhelper);
//        $newhelper->fromState($this->obvious($oldhelper));
//        while ($oldhelper->bulbs() != $newhelper->bulbs()) {
//            $newhelper->setRoot($oldhelper);
//            array_push($this->solution, $newhelper);
//            $oldhelper = $newhelper;
//            $newhelper = new GameState();
//            $newhelper->fromState($this->obvious($oldhelper));
//        }
//        array_push($this->search_space, $newhelper);
        array_push($this->search_space, $oldhelper);
        return $this->solve();
    }

    private function obvious($state)
    {
        $helper = new GameState();
        $helper->fromState($state);
        foreach($helper->special() as $field)
        {
            if($field->canHave() == $field->need())
            {
                foreach($field->neighbors() as $neighbor)
                {
                    if($neighbor != null)
                        $neighbor->lightUp();
                }
            }

        }
        return $helper;
    }

    public function solve()
    {
        $lastid = 0;
        while(count($this->search_space) > 0)
        {
            $current = $this->search_space[$lastid];
            if($current->unlit() == 0 && $current->need() == 0) {
                $result = array();
                while($current->root() != null)
                {
                    array_push($result, $current);
                    $current = $current->root();
                }
                $result = array_reverse($result);
                $this->solution = array_merge($this->solution,$result);
                return $this->solution;
            }

            unset($children);
            $children = $current->generateChildren();

            if($children != null)
                $this->addToSearch($children);

            array_push($this->visited, $current);
            unset($this->search_space[$lastid]);

            $lowest = PHP_INT_MAX;
            foreach($this->search_space as $key => $tested)
            {
                //unlit + need as simple heuristic
                $heuristic = $tested->unlit() + ($tested->need() * 100);
                $tested->current_cost = $tested->root()->current_cost + $tested->cost;
                if ($tested->current_cost + $heuristic < $lowest) {
                    $lowest = $tested->current_cost + $heuristic;
                    $lastid = $key;
                }
            }
        }
    }

}