<?php
/**
 * Created by PhpStorm.
 * User: markownik
 * Date: 26/03/17
 * Time: 18:12
 */

ini_set('memory_limit', '4096M');

require_once 'AStarSolver.class.php';

if($argc < 2) {
    echo "Please supply input json file as argument";
}

$text = file_get_contents($argv[1]);

$json = json_decode($text);
$table = $json[0];
$parameters = $json[1];

$start = time(NULL);
$state = new lightupsolver\GameState();
$state->fromArray($table);

$solver = new lightupsolver\AStarSolver();
$solution_path = $solver->solution($state);
$finish = time(NULL);

$solution_table = $solution_path[count($solution_path)-1]->toArray();
$json = [$solution_table, $parameters];
$text = json_encode($json);

$arr = explode('.', $argv[1]);
$arr[count($arr)-2] .= "_solution";
$outpath = implode('.', $arr);
file_put_contents($outpath, $text);

echo "\nSolving took ".($finish-$start)."s.\n";
echo "Solution path: \n\n";

foreach($solution_path as $step => $result)
{
    if($step == 0) $text="Initial "; else $text="Step ".$step;
    if($step == count($solution_path)-1) $text="Final Solution";
    echo "    ".$text." ".$result->toString()."\n";
}


