<?php
/**
 * Created by PhpStorm.
 * User: markownik
 * Date: 08/04/17
 * Time: 13:07
 */

namespace lightupsolver;


abstract class FieldState
{
    const NotLit = 0;
    const Lit = 1;
    const LightBulb = 2;
}