<?php
/**
 * Created by PhpStorm.
 * User: markownik
 * Date: 08/04/17
 * Time: 10:55
 */
namespace lightupsolver;

require_once "Field.class.php";


class GameState
{
    private $_table = array(array());
    private $_unlit = array();
    private $_special = array();
    private $_children = array();
    private $_root;
    private $_size;
    private $_need;
    private $_bulbs;
    private $_lit;
    public $current_cost = 0;
    public $cost = 1;

    private function update()
    {
        $this->_need = 0;
        $this->_bulbs = 0;
        $this->_lit = 0;
        $this->_unlit = array();
        for($x=0;$x<$this->_size;$x++)
        {
            for($y=0;$y<$this->_size;$y++)
            {
                if($this->_table[$x][$y]->need() > -1)
                    $this->_need += $this->_table[$x][$y]->need();
                if($this->_table[$x][$y]->type() == FieldType::Normal)
                {
                    if ($this->_table[$x][$y]->isLit() != FieldState::NotLit)
                    {
                        $this->_lit++;
                        if ($this->_table[$x][$y]->isLit() == FieldState::LightBulb)
                            $this->_bulbs++;
                    }
                    else
                    {
                        array_push($this->_unlit, $this->_table[$x][$y]);
                    }
                }
            }
        }
    }

    private function populateNeighbors()
    {
        for($x=0;$x<$this->_size;$x++) {
            for ($y = 0; $y < $this->_size; $y++) {
                $left = (isset($this->_table[$x-1][$y]))?$this->_table[$x-1][$y]:null;
                $up = (isset($this->_table[$x][$y-1]))?$this->_table[$x][$y-1]:null;
                $right = (isset($this->_table[$x+1][$y]))?$this->_table[$x+1][$y]:null;
                $down = (isset($this->_table[$x][$y+1]))?$this->_table[$x][$y+1]:null;
                $this->_table[$x][$y]->setNeighbor(Direction::Left, $left);
                $this->_table[$x][$y]->setNeighbor(Direction::Up, $up);
                $this->_table[$x][$y]->setNeighbor(Direction::Right, $right);
                $this->_table[$x][$y]->setNeighbor(Direction::Down, $down);
            }
        }
    }

    public function __construct()
    {
        $this->_size = 0;
        $this->_root = null;
    }

    public function fromState($state)
    {
        $this->_size = $state->_size;
        $this->_special = array();
        for($x=0;$x<$this->_size;$x++) {
            for ($y = 0; $y < $this->_size; $y++) {
                $old = $state->getFieldXY($x,$y);
                $this->_table[$x][$y] = new Field(FieldType::Normal,-1);
                $this->_table[$x][$y]->fromField($old);
                if($this->_table[$x][$y]->value() > -1)
                    array_push($this->_special, $this->_table[$x][$y]);
            }
        }
        $this->populateNeighbors();
        $this->update();
    }

    public function fromArray($array)
    {
        $this->_size = sizeof($array);
        foreach($array as $row_key => $row)
        {
            foreach($row as $column_key => $value)
            {

                if($value == "b")
                {
                    $this->_table[$column_key][$row_key] = new Field(FieldType::Black, -1);
                }
                elseif(in_array($value,["0","1","2","3","4"]))
                {
                    $this->_table[$column_key][$row_key] = new Field(FieldType::Black, intval($value));
                    array_push($this->_special, $this->_table[$column_key][$row_key]);
                }
                else
                {
                    $this->_table[$column_key][$row_key] = new Field(FieldType::Normal, -1);
                }
                $xy = new Coord();
                $xy -> fromXY($column_key,$row_key);
                $this->_table[$column_key][$row_key]->setCoord($xy);
            }
        }
        $this->populateNeighbors();
        $this->update();
    }

    public function toArray()
    {
        $result = array(array());
        foreach ($this->_table as $row_key => $row) {
            foreach ($row as $column_key => $field) {
                $val = "";
                if($field->type() == FieldType::Black) {
                    $val = $field->toString();
                    ($val == "#")?$val="b":$val;
                }
                else
                {
                    $val = $field->toString();
                    ($val == "." || $val == " ")?$val="":$val;
                }
                $result[$column_key][$row_key] = $val;
            }
        }
        return $result;
    }

    public function generateChildren()
    {
        $children = array();
        for($id=0;$id<count($this->_unlit);$id++)
        {
            $child = new GameState();
            $child->fromState($this);
            $child->_root = $this;
            $child->lightUpCoord($this->_unlit[$id]->getCoord());
            if($this->bulbs() != $child->bulbs())
                array_push($children, $child);
            unset($child);
        }
        unset($this->_children);
        $this->_children = $children;

        return $this->_children;
    }

    public function children()
    {
        return $this->_children;
    }

    public function root()
    {
        return $this->_root;
    }

    public function setRoot($root)
    {
        $this->_root = $root;
    }

    public function need()
    {
        return $this->_need;
    }

    public function bulbs()
    {
        return $this->_bulbs;
    }

    public function lit()
    {
        return $this->_lit;
    }

    public function unlit()
    {
        return count($this->_unlit);
    }

    public function size()
    {
        return $this->_size;
    }

    public function special()
    {
        return $this->_special;
    }

    public function lightUpCoord($coord)
    {
        $this->_table[$coord->x][$coord->y]->lightUp();
        $this->update();
    }

    public function getFieldXY($x, $y)
    {
        return $this->_table[$x][$y];
    }

    public function toString()
    {
        $ret = "";
        $ret .= "Gamestate:\n";
        //$ret .= "\tneed:\t".$this->_need."\n";
        //$ret .= "\tlit:\t".$this->_lit."\n";
        //$ret .= "\tbulbs:\t".$this->_bulbs."\n";

        $ret .= "\n\t|";
        for($x = 0; $x < $this->_size; $x++) {
            $ret .= "----|";
        }
        $ret .= "\n";

        for($y = 0; $y < $this->_size; $y++)
        {
            $ret .= "\t|  ";
            for($x = 0; $x < $this->_size; $x++)
            {
                $ret .= $this->_table[$x][$y]->toString()." |  ";
            }
            $ret .= "\n";

            $ret .= "\t|";
            for($x = 0; $x < $this->_size; $x++) {
                $ret .= "----|";
            }
            $ret .= "\n";
        }
        return $ret;
    }
}