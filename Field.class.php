<?php
/**
 * Created by PhpStorm.
 * User: markownik
 * Date: 08/04/17
 * Time: 11:09
 */

namespace lightupsolver;

require_once "FieldState.class.php";
require_once "FieldType.class.php";
require_once "Direction.class.php";
require_once "Coord.class.php";


class Field
{
    private $_is_lit;
    private $_type;
    private $_neighbors;
    private $_need;
    private $_have;
    private $_coord;
    private $_value;

    public function __construct($type, $value)
    {
        $this->_type = $type;
        if ($type == FieldType::Black && $value > -1)
        {
            $this->_need = $value;
            $this->_value = $value;
            $this->_have = 0;
        }
        else
        {
            $this->_need = -1;
            $this->_value = -1;
            $this->_have = -1;
        }
        $this->_is_lit = FieldState::NotLit;
        $this->_neighbors = Array(null, null, null, null);
    }

    public function fromField($old)
    {
        $this->_is_lit = $old->_is_lit;
        $this->_type = $old->_type;
        $this->_need = $old->_need;
        $this->_value = $old->_value;
        $this->_have = $old->_have;
        $this->_coord = $old->_coord;
    }

    /**
     * @return int
     */
    public function isLit()
    {
        return $this->_is_lit;
    }

    public function lightUp()
    {
        if($this->canLightUp())
        {
            $this->_is_lit = FieldState::LightBulb;
            foreach ($this->_neighbors as $dir => $neighbor)
            {
                if ($neighbor != null)
                {
                    $neighbor->propagateLight($dir);
                }
            }
        }
    }

    public function canLightUp()
    {
        foreach ($this->_neighbors as $dir => $neighbor) {
            if ($neighbor != null) {
                if($neighbor->need() == 0)
                {
                    return false;
                }
            }
        }
        if($this->_is_lit == FieldState::NotLit && $this->_type == FieldType::Normal)
            return true;
        else
            return false;
    }

    public function propagateLight($dir)
    {
        if($this->_is_lit != FieldState::LightBulb && $this->_type == FieldType::Normal)
        {
            $this->_is_lit = FieldState::Lit;
            if ($this->_neighbors[$dir] != null)
                $this->_neighbors[$dir]->propagateLight($dir);
        }
        elseif ($this->_type == FieldType::Black &&
            $this->_neighbors[Direction::swap($dir)]->isLit() == FieldState::LightBulb)
        {
            if ($this->_need > -1)
                $this->_have++;
        }
    }

    public function type()
    {
        return $this->_type;
    }

    public function value()
    {
        return $this->_value;
    }

    public function setNeighbor($dir, $neighbor)
    {
        $this->_neighbors[$dir] = $neighbor;
    }

    public function neighbors()
    {
        return $this->_neighbors;
    }

    public function need()
    {
        if ($this->_need > -1)
            return $this->_need - $this->_have;
        else
            return -1;
    }

    public function have()
    {
        return $this->_have;
    }

    public function canHave()
    {
        $count=0;
        for($i=0;$i<=3;$i++)
        {
            if($this->_neighbors[$i] != null && $this->_neighbors[$i]->canLightUp())
                $count++;
        }
        return $count;
    }

    public function getCoord()
    {
        return $this->_coord;
    }

    public function setCoord($coord)
    {
        $this->_coord = $coord;
    }

    public function toString()
    {
        if ($this->type() == FieldType::Black)
        {
            if($this->need() > -1)
                return ''.$this->_value.'';
            else
                return '#';
        }
        if ($this->type() == FieldType::Normal)
            if($this->isLit() == FieldState::LightBulb)
                return '*';
            elseif($this->isLit() == FieldState::Lit)
                return '.';
            else
                return ' ';
    }
}