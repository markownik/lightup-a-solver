<?php
/**
 * Created by PhpStorm.
 * User: markownik
 * Date: 08/04/17
 * Time: 13:10
 */

namespace lightupsolver;


abstract class Direction
{
    const Left = 0;
    const Up = 1;
    const Right = 2;
    const Down = 3;

    public function Swap($dir)
    {
        if($dir - 2 < 0)
            return $dir + 2;
        else
            return $dir - 2;
    }
}