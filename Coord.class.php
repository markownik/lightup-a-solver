<?php
/**
 * Created by PhpStorm.
 * User: markownik
 * Date: 08/04/17
 * Time: 15:03
 */

namespace lightupsolver;


class Coord
{
    public $x;
    public $y;

    public function __construct()
    {
        $this->x = 0;
        $this->y = 0;
    }

    public function fromCoord($coord)
    {
        $this->x = $coord->x;
        $this->y = $coord->y;
    }

    public function fromXY($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }
}